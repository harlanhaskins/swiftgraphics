// swift-tools-version:4.1

import PackageDescription

let package = Package(
  name: "SwiftGraphics",
  dependencies: [
    .package(url: "https://harlanhaskins@bitbucket.org/harlanhaskins/clibglfw.git", .branch("master")),
    .package(url: "https://github.com/harlanhaskins/VectorMath.git", .branch("8-bit-synthesizer")),
  ],
  targets: [
    .target(
      name: "SwiftGraphics",
      dependencies: ["VectorMath"]
    ),
  ]
  )

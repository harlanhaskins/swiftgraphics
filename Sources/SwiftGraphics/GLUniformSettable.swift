import Clibglfw

public protocol GLUniformSettable {
  func set(at loc: GLint)
}

private func setShaderValue<T>(
  _ loc: GLint,
  value: T,
  setter: @convention(c) (GLint, GLsizei, UnsafePointer<GLfloat>?) -> Void) {
  var val = value
  let count = MemoryLayout<T>.size / MemoryLayout<Float>.size
  withUnsafePointer(to: &val) { ptr in
    ptr.withMemoryRebound(to: Float.self, capacity: count) {
      setter(loc, 1, $0)
    }
  }
}

extension vec2: GLUniformSettable {
  public func set(at loc: GLint) {
    setShaderValue(loc, value: self, setter: glUniform2fv)
  }
}

extension vec3: GLUniformSettable {
  public func set(at loc: GLint) {
    setShaderValue(loc, value: self, setter: glUniform3fv)
  }
}

extension vec4: GLUniformSettable {
  public func set(at loc: GLint) {
    setShaderValue(loc, value: self, setter: glUniform4fv)
  }
}

extension mat3: GLUniformSettable {
  public func set(at loc: GLint) {
    setShaderValue(loc, value: self) {
      glUniformMatrix3fv($0, $1, 0, $2)
    }
  }
}

extension Bool: GLUniformSettable {
  public func set(at loc: GLint) {
    glUniform1i(loc, self ? 0 : 1)
  }
}

extension GLint: GLUniformSettable {
  public func set(at loc: GLint) {
    glUniform1i(loc, self)
  }
}

extension GLuint: GLUniformSettable {
  public func set(at loc: GLint) {
    glUniform1i(loc, GLint(self))
  }
}

extension Float: GLUniformSettable {
  public func set(at loc: GLint) {
    glUniform1f(loc, self)
  }
}

extension mat4: GLUniformSettable {
  public func set(at loc: GLint) {
    setShaderValue(loc, value: self) {
      glUniformMatrix4fv($0, $1, 0, $2)
    }
  }
}

import VectorMath

public struct Camera: Transformable {
  public var transform = Transform()

  var viewMatrix: mat4 {
    return transform.matrix.inverse
  }
}

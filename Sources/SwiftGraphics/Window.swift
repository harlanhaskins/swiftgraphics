import Clibglfw
import OpenGL
import AppKit

public class Window {
  private let handle: OpaquePointer
  let width: Int
  let height: Int

  var scene: Scene?

  init?(title: String, width: Int, height: Int) {
    self.width = width
    self.height = height
    // TODO: Support monitors?
    guard let w = glfwCreateWindow(Int32(width), Int32(height), title, nil, nil) else {
      return nil
    }
    handle = w
    glfwMakeContextCurrent(handle)
  }

  var aspectRatio: Float {
    return Float(width) / Float(height)
  }

  public func loop(_ update: ((Double) -> Void)? = nil) {
    while glfwWindowShouldClose(handle) == 0 {
      scene?.draw()
      update?(glfwGetTime())
      glfwSwapBuffers(handle)
      glfwPollEvents()
    }
  }

  public func waitForEvents() {
    glfwWaitEvents()
  }

  public func show() {
    glfwShowWindow(handle)
  }

  public func hide() {
    glfwHideWindow(handle)
  }

  public func focus() {
    glfwFocusWindow(handle)
  }

  deinit {
    glfwDestroyWindow(handle)
  }
}

import Foundation
import Clibglfw
import VectorMath

public class Shader {
  enum Error: Swift.Error, CustomStringConvertible {
    case vertexLoad, fragmentLoad, vertexCompile, fragmentCompile, link

    var description: String {
      switch self {
      case .vertexLoad: return "Error loading vertex shader"
      case .fragmentLoad: return "Error loading fragment shader"
      case .vertexCompile: return "Error compiling vertex shader"
      case .fragmentCompile: return "Error compiling fragment shader"
      case .link: return "Error linking shader"
      }
    }
  }
  public struct Parameter<T: GLUniformSettable>: RawRepresentable {
    public let rawValue: String
    public init(rawValue: String) {
      self.rawValue = rawValue
    }

    static var vertexPosition: Parameter<vec3> {
      return .init(rawValue: "vPosition")
    }

    static var vertexNormal: Parameter<vec3> {
      return .init(rawValue: "vNormal")
    }

    static var vertexTextureCoordinate: Parameter<vec2> {
      return .init(rawValue: "vTexCoord")
    }
  }

  let program: GLuint

  private static func printProgramInfoLog(_ handle: GLuint) {
    var length: GLint = 0
    glGetProgramiv(handle, GLenum(GL_INFO_LOG_LENGTH), &length)
    if length == 0 { return }
    let ptr = UnsafeMutablePointer<GLchar>.allocate(capacity: Int(length))
    glGetProgramInfoLog(handle, length, nil, ptr)
    print("Program log:\n")
    print("\(String(cString: ptr))")
    ptr.deallocate()
  }

  private static func printShaderInfoLog(_ handle: GLuint) {
    var length: GLint = 0
    glGetShaderiv(handle, GLenum(GL_INFO_LOG_LENGTH), &length)
    if length == 0 { return }
    let ptr = UnsafeMutablePointer<GLchar>.allocate(capacity: Int(length))
    glGetShaderInfoLog(handle, length, nil, ptr)
    print("Shader log:\n")
    print("\(String(cString: ptr))")
    ptr.deallocate()
  }

  private static func loadAndCompile(
    _ url: URL,
    _ type: Int32,
    ifLoadFail: Error,
    ifCompileFail: Error
  ) throws -> GLuint {
    let data: Data
    do {
      data = try Data(contentsOf: url, options: .mappedIfSafe)
    } catch {
      throw ifLoadFail
    }
    let handle = glCreateShader(GLenum(type))
    try data.withUnsafeBytes { (ptr: UnsafePointer<Int8>) in
      var p = ptr as Optional
      var flag = GLint()
      glShaderSource(handle, 1, &p, nil)
      glCompileShader(handle)
      glGetShaderiv(handle, GLenum(GL_COMPILE_STATUS), &flag)
      printShaderInfoLog(handle)
      if flag == GL_FALSE {
        throw ifCompileFail
      }
    }
    return handle
  }

  public init(vertex: URL, fragment: URL) throws {
    let vertexHandle = try Shader.loadAndCompile(
      vertex,
      GL_VERTEX_SHADER,
      ifLoadFail: .vertexLoad,
      ifCompileFail: .vertexCompile
    )
    let fragmentHandle = try Shader.loadAndCompile(
      fragment,
      GL_FRAGMENT_SHADER,
      ifLoadFail: .fragmentLoad,
      ifCompileFail: .fragmentCompile
    )

    program = glCreateProgram()
    glAttachShader(program, vertexHandle)
    glAttachShader(program, fragmentHandle)
    Shader.printProgramInfoLog(program)

    glLinkProgram(program)
    var flag = GLint()
    glGetProgramiv(program, GLenum(GL_LINK_STATUS), &flag)
    Shader.printProgramInfoLog(program)
    if flag == GL_FALSE {
      throw Error.link
    }
  }

  public func uniformLocation(name: String) -> GLint {
    return glGetUniformLocation(program, name)
  }

  public func setUniform<T: GLUniformSettable>(_ param: Parameter<T>, value: T) {
    let loc = uniformLocation(name: param.rawValue)
    value.set(at: loc)
  }
}

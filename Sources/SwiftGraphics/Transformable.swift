import VectorMath

public protocol Transformable {
  var transform: Transform { get set }
}

extension Transformable {
  public mutating func translateBy(_ translation: vec3) {
    transform.translateBy(translation)
  }

  public mutating func rotateBy(_ rotation: vec3) {
    transform.rotateBy(rotation)
  }

  public mutating func scaleBy(_ scale: vec3) {
    transform.scaleBy(scale)
  }

  public mutating func rotateBy(degrees: vec3) {
    transform.rotateBy(degrees.radians)
  }
}

import Foundation
import VectorMath

extension Mesh {
  static func alongCircle(_ y: Float, _ radius: Float, _ rads: Float) -> Vertex {
    return Vertex(position: vec3(sin(rads) * radius, y, cos(rads) * radius))
  }
  /// Create polygons for a cylinder with unit height and diameter,
  /// centered at the origin, with separate number of radial subdivisions and
  /// height subdivisions.
  ///
  /// - Parameter radialDivision: number of subdivisions on the radial base
  /// - Parameter heightDivisions: number of subdivisions along the height
  static func cylinder(radialDivisions: Int, heightDivisions: Int) -> Mesh {
    let radialDivisions = max(radialDivisions, 3)
    let heightDivisions = max(heightDivisions, 1)

    let m = Mesh()
    let radius: Float = 0.5

    let topCenter = Vertex(position: vec3(0, 0.5, 0))
    let bottomCenter = Vertex(position: vec3(0, -0.5, 0))
    let angleDiff = (2 * .pi) / Float(radialDivisions)
    var angle: Float = 0.0
    while angle < 2 * .pi {
      defer { angle += angleDiff }
      let topV1 = alongCircle(0.5, radius, angle)
      let topV2 = alongCircle(0.5, radius, angle + angleDiff)
      let bottomV1 = alongCircle(-0.5, radius, angle + angleDiff)
      let bottomV2 = alongCircle(-0.5, radius, angle)
      m.addTriangle(Triangle(topCenter, topV1, topV2))
      m.addTriangle(Triangle(bottomCenter, bottomV1, bottomV2))

      for i in 0..<heightDivisions {
        let nextY = Float(i + 1) / Float(heightDivisions)
        let thisY = Float(i) / Float(heightDivisions)
        var lr = topV2
        lr.position.y -= 1.0 - thisY
        var ur = topV2
        ur.position.y -= 1.0 - nextY
        var ul = topV1
        ul.position.y -= 1.0 - nextY
        var ll = topV1
        ll.position.y -= 1.0 - thisY
        m.addQuad(lr, ur, ul, ll)
      }
    }
    return m
  }
}

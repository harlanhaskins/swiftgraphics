import Clibglfw
import VectorMath

internal enum ShaderData {
  struct Locked {
    let positions: UnsafeBufferPointer<vec3>
    let normals: UnsafeBufferPointer<vec3>
    let textureCoordinates: UnsafeBufferPointer<vec2>
    let elements: UnsafeBufferPointer<GLuint>

    func destroy() {
      positions.deallocate()
      normals.deallocate()
      textureCoordinates.deallocate()
      elements.deallocate()
    }
  }

  class Unlocked {
    var positions = [vec3]()
    var normals = [vec3]()
    var textureCoordinates = [vec2]()
    var elements = [GLuint]()
    var elementMap = [Vertex: GLuint]()

    func add(_ vertex: Vertex) {
//      if let elt = elementMap[vertex] {
//        elements.append(elt)
//        return
//      }
      let elt = GLuint(positions.count)
      elementMap[vertex] = elt
      elements.append(elt)
      positions.append(vertex.position)
      normals.append(vertex.normal)
      textureCoordinates.append(vertex.textureCoordinate)
    }

    func lock() -> Locked {
      return Locked(
        positions: positions.makeBuffer(),
        normals: normals.makeBuffer(),
        textureCoordinates: textureCoordinates.makeBuffer(),
        elements: elements.makeBuffer()
      )
    }
  }
  case unlocked(Unlocked)
  case locked(Locked, vertexBuffer: GLuint, elementBuffer: GLuint)
}

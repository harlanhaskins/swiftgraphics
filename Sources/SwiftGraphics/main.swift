import Clibglfw
import Foundation
import VectorMath

func main() throws -> Int {
  glfwSetErrorCallback { code, desc in
    let str = String(cString: desc!)
    print("GLFW error \(code): \(str)")
    exit(1)
  }

  if glfwInit() == 0 {
    print("Can't initialize GLFW!")
    return 1
  }
  defer { glfwTerminate() }

  guard let window = Window(title: "Test", width: 720, height: 480) else {
    print("Could not create GLFW window")
    return 1
  }

  glEnable(GLenum(GL_DEPTH_TEST))
  glPolygonMode(GLenum(GL_FRONT_AND_BACK), GLenum(GL_FILL))
  glClearDepth(10.0)

  var material = PhongMaterial()
  material.color = vec3(1, 0, 0)

  var camera = Camera()
  camera.translateBy(vec3(0, 0, 10))
  camera.rotateBy(degrees: vec3(0, 0, 0))

  let light = Light(position: vec3(2, 2, 2), color: vec3(1, 1, 1))
  let projection = Projection(
    kind: .frustum,
    fieldOfViewY: 60.0,
    aspectRatio: window.aspectRatio,
    near: 0.001,
    far: 100.0
  )
  var model = Model(mesh: .cone(radialDivisions: 36, heightDivisions: 10), material: material)

  window.scene = Scene(camera: camera, light: light, projection: projection)
  window.scene?.addModel(model)
  window.loop { _ in
    model.rotateBy(vec3(0.01, 0.05, 0.025))
  }

  return 0
}

extension mat4: CustomStringConvertible {
  public var description: String {
    var s = ""
    for (offset, value) in toArray().enumerated() {
      if offset != 0 && offset % 4 == 0 {
        s += "\n"
      }
      s += "\(value)\t\t"
    }
    return s + "\n"
  }
}

do {
  exit(Int32(try main()))
} catch {
  print("error: \(error)")
}

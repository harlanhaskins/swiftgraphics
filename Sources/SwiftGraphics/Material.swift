import Clibglfw
import Foundation
import VectorMath

let resourcesDir = URL(fileURLWithPath: #file) // Material.swift
  .deletingLastPathComponent() // SwiftGraphics
  .deletingLastPathComponent() // Sources
  .deletingLastPathComponent() // root
  .appendingPathComponent("Resources")

public protocol Material {
  var shader: Shader { get }
  func setProperties()
}

public struct PhongMaterial: Material {
  public var shader: Shader
  public var color: vec3
  public var ambient: Float
  public var diffuse: Float
  public var specular: Float
  public var specularExponent: Float
  public var textureScaling: vec2

  // TODO: texture

  public init(
    color: vec3 = vec3(1, 1, 1),
    ambient: Float = 0.5,
    diffuse: Float = 0.7,
    specular: Float = 1.0,
    specularExponent: Float = 64.0,
    textureScaling: vec2 = vec2(1, 1)
  ) {
    self.shader = try! Shader(
      vertex: resourcesDir.appendingPathComponent("phong.vert"),
      fragment: resourcesDir.appendingPathComponent("phong.frag")
    )
    self.color = color
    self.ambient = ambient
    self.diffuse = diffuse
    self.specular = specular
    self.specularExponent = specularExponent
    self.textureScaling = textureScaling
  }

  public func setProperties() {
    shader.setUniform(.materialColor, value: color)
    shader.setUniform(.ambientCoefficient, value: ambient)
    shader.setUniform(.diffuseCoefficient, value: diffuse)
    shader.setUniform(.specularCoefficient, value: specular)
    shader.setUniform(.specularExponent, value: specularExponent)
  }
}

extension Shader.Parameter {
  static var materialColor: Shader.Parameter<vec3> {
    return Shader.Parameter(rawValue: "materialColor")
  }
  static var ambientCoefficient: Shader.Parameter<Float> {
    return Shader.Parameter(rawValue: "ambientCoeff")
  }
  static var diffuseCoefficient: Shader.Parameter<Float> {
    return Shader.Parameter(rawValue: "diffuseCoeff")
  }
  static var specularCoefficient: Shader.Parameter<Float> {
    return Shader.Parameter(rawValue: "specCoeff")
  }
  static var specularExponent: Shader.Parameter<Float> {
    return Shader.Parameter(rawValue: "specExp")
  }
  static var texture: Shader.Parameter<GLint> {
    return Shader.Parameter(rawValue: "texture")
  }
  static var textureScaling: Shader.Parameter<GLint> {
    return Shader.Parameter(rawValue: "textureScaling")
  }
}

import Foundation
import VectorMath

private let A: Float = (0.6180339887 / 2)

private let v = [
  vec3( 0,  A, -0.5),
  vec3(-A,  0.5,  0),
  vec3( A,  0.5,  0),
  vec3( 0,  A,  0.5),
  vec3(-0.5,  0,  A),
  vec3( 0, -A,  0.5),
  vec3( 0.5,  0,  A),
  vec3( 0.5,  0, -A),
  vec3( 0, -A, -0.5),
  vec3(-0.5,  0, -A),
  vec3(-A, -0.5,  0),
  vec3( A, -0.5,  0)
]

private let triangles = [
  Triangle(v[0], v[1], v[2]),
  Triangle(v[3], v[2], v[1]),
  Triangle(v[3], v[4], v[5]),
  Triangle(v[3], v[5], v[6]),
  Triangle(v[0], v[7], v[8]),
  Triangle(v[0], v[8], v[9]),
  Triangle(v[5], v[10], v[11]),
  Triangle(v[8], v[11], v[10]),
  Triangle(v[1], v[9], v[4]),
  Triangle(v[10], v[4], v[9]),
  Triangle(v[2], v[6], v[7]),
  Triangle(v[11], v[7], v[6]),
  Triangle(v[3], v[1], v[4]),
  Triangle(v[3], v[6], v[2]),
  Triangle(v[0], v[9], v[1]),
  Triangle(v[0], v[2], v[7]),
  Triangle(v[8], v[10], v[9]),
  Triangle(v[8], v[7], v[11]),
  Triangle(v[5], v[4], v[10]),
  Triangle(v[5], v[11], v[6])
]

extension Mesh {
  /// I spent so much time on this.
  func computeSphericalAttrs(_ v: inout Vertex) {
    v.normal = v.position

    let pos = v.position

    var theta: Float = atan2(pos.z, pos.x)
    if pos.z < 0 {
      theta += 2 * .pi
    }
    v.textureCoordinate.x = theta / (2 * .pi)

    let phi = asin(pos.y)
    v.textureCoordinate.y = (phi / .pi) + 0.5
  }

  func midpoint(_ aVertex: Vertex, _ bVertex: Vertex) -> Vertex {
    let a = aVertex.position
    let b = bVertex.position
    return Vertex(
      position: vec3(
        a.x + ((b.x - a.x) * 0.5),
        a.y + ((b.y - a.y) * 0.5),
        a.z + ((b.z - a.z) * 0.5)
      )
    )
  }

  func subdivideTriangle(_ triangle: Triangle, subdivisions: Int) {
    let v1 = triangle.a
    let v2 = triangle.b
    let v3 = triangle.c

    if subdivisions == 1 {
      var t1 = Vertex(position: v1.position.normalized())
      computeSphericalAttrs(&t1)
      var t2 = Vertex(position: v2.position.normalized())
      computeSphericalAttrs(&t2)
      var t3 = Vertex(position: v3.position.normalized())
      computeSphericalAttrs(&t3)
      addTriangle(Triangle(t1, t2, t3))
      return
    }

  /* Divide this triangle into 4 subtriangles. (Bad ASCII art).

   ----------------
   \      /\      /
   \    /  \    /
   \  /    \  /
   \/      \/
   \------/
   \    /
   \  /
   \/
   */
    subdivideTriangle(
      Triangle(v1, midpoint(v1, v2), midpoint(v1, v3)),
      subdivisions: subdivisions - 1
    )
    subdivideTriangle(
      Triangle(midpoint(v1, v2), v2, midpoint(v2, v3)),
      subdivisions: subdivisions - 1
    )
    subdivideTriangle(
      Triangle(midpoint(v1, v3), midpoint(v2, v3), v3),
      subdivisions: subdivisions - 1
    )
    subdivideTriangle(
      Triangle(midpoint(v1, v3), midpoint(v1, v2), midpoint(v2, v3)),
      subdivisions: subdivisions - 1
    )
  }

  ///
  // makeSphere - Create sphere of a given radius, centered at the origin,
  // using spherical coordinates with separate number of thetha and
  // phi subdivisions.
  //
  // @param radius - Radius of the sphere
  // @param slices - number of subdivisions in the theta direction
  // @param stacks - Number of subdivisions in the phi direction.
  ///
  static func sphere(subdivisions: Int) -> Mesh {
    let m = Mesh()
    let subdivisions = min(max(subdivisions, 1), 7)

    for tri in triangles {
      m.subdivideTriangle(tri, subdivisions: subdivisions)
    }

    return m
  }
}

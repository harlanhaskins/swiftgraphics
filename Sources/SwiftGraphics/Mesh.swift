import Clibglfw
import VectorMath

extension Array {
  func makeBuffer() -> UnsafeBufferPointer<Element> {
    let base = UnsafeMutableBufferPointer<Element>.allocate(capacity: count)
    _ = base.initialize(from: self)
    return UnsafeBufferPointer<Element>(start: base.baseAddress, count: base.count)
  }
}

extension UnsafeBufferPointer {
  var byteSize: Int {
    return count * MemoryLayout<Element>.size
  }
}

public class Mesh {
  private var vertices = ShaderData.unlocked(.init())

  deinit {
    if case .locked(let vertices, _, _) = vertices {
      vertices.destroy()
    }
  }

  public func add(_ vertex: Vertex) {
    switch vertices {
    case .unlocked(let data):
      data.add(vertex)
    case .locked:
      fatalError("Cannot add vertices to a locked mesh.")
    }
  }

  func addTriangle(_ triangle: Triangle, inferNormals: Bool = false) {
    var triangle = triangle
    if inferNormals {
      triangle.computeNormals()
    }
    add(triangle.a)
    add(triangle.b)
    add(triangle.c)
  }

  func addQuad(_ lr: Vertex, _ ur: Vertex, _ ul: Vertex, _ ll: Vertex,
               inferNormals: Bool = false) {
    addTriangle(Triangle(lr, ur, ul), inferNormals: inferNormals)
    addTriangle(Triangle(lr, ul, ll), inferNormals: inferNormals)
  }

  func addQuad(_ lr: vec3, _ ur: vec3, _ ul: vec3, _ ll: vec3,
               inferNormals: Bool = false) {
    addQuad(
      Vertex(position: lr, textureCoordinate: vec2(1, 1)),
      Vertex(position: ur, textureCoordinate: vec2(1, 0)),
      Vertex(position: ul, textureCoordinate: vec2(0, 0)),
      Vertex(position: ll, textureCoordinate: vec2(0, 1)),
      inferNormals: inferNormals
    )
  }

  func makeBuffer(target: GLenum, data: UnsafeRawPointer?, size: GLsizeiptr) -> GLuint {
    var buf = GLuint()
    glGenBuffers(1, &buf)
    glBindBuffer(target, buf)
    glBufferData(target, size, data, GLenum(GL_STATIC_DRAW))
    return buf
  }

  func setArraySubdata<T: GLUniformSettable>(
    _ parameter: Shader.Parameter<T>,
    _ offset: inout GLintptr,
    _ parameters: UnsafeBufferPointer<T>
    ) {
    let totalSize = parameters.byteSize
    glBufferSubData(
      GLenum(GL_ARRAY_BUFFER),
      offset,
      totalSize,
      parameters.baseAddress
    )
    offset += totalSize
  }

  func enableVertexArray<T: GLUniformSettable>(
    _ program: GLuint,
    _ parameter: Shader.Parameter<T>,
    _ offset: inout GLintptr,
    _ parameters: UnsafeBufferPointer<T>
  ) {
    let totalSize = parameters.byteSize
    let loc = GLuint(glGetAttribLocation(program, parameter.rawValue))
    glVertexAttribPointer(
      loc,
      GLint(MemoryLayout<T>.size / MemoryLayout<Float>.size),
      GLenum(GL_FLOAT),
      GLboolean(GL_FALSE),
      GLsizei(MemoryLayout<T>.size),
      UnsafeRawPointer(bitPattern: offset)
    )
    glEnableVertexAttribArray(loc)
    offset += totalSize
  }

  func finalize() {
    // If you finalize twice, no big deal.
    guard case .unlocked(let data) = vertices else { return }
    let locked = data.lock()

    let posSize = locked.positions.byteSize
    let normSize = locked.normals.byteSize
    let textSize = locked.textureCoordinates.byteSize
    let totalSize = posSize + normSize + textSize

    let vertexBuffer = makeBuffer(
      target: GLenum(GL_ARRAY_BUFFER),
      data: nil,
      size: totalSize
    )

    var offset = GLintptr()
    setArraySubdata(.vertexPosition, &offset, locked.positions)
    setArraySubdata(.vertexNormal, &offset, locked.normals)
    setArraySubdata(.vertexTextureCoordinate, &offset, locked.textureCoordinates)

    let elementBuffer = makeBuffer(
      target: GLenum(GL_ELEMENT_ARRAY_BUFFER),
      data: locked.elements.baseAddress,
      size: locked.elements.byteSize
    )

    self.vertices = .locked(
      locked,
      vertexBuffer: vertexBuffer,
      elementBuffer: elementBuffer
    )
  }

  func draw(in shader: Shader) {
    guard case .locked(let vertices, let vertexBuffer, let elementBuffer) = vertices else {
      fatalError("Cannot draw before finalizing; call finalize() before draw(in:)")
    }
    glBindBuffer(GLenum(GL_ARRAY_BUFFER), vertexBuffer)
    glBindBuffer(GLenum(GL_ELEMENT_ARRAY_BUFFER), elementBuffer)
    var offset = GLintptr()
    enableVertexArray(shader.program, .vertexPosition, &offset, vertices.positions)
    enableVertexArray(shader.program, .vertexNormal, &offset, vertices.normals)
    enableVertexArray(shader.program, .vertexTextureCoordinate, &offset, vertices.textureCoordinates)
    glDrawElements(
      GLenum(GL_TRIANGLES),
      GLsizei(vertices.elements.count),
      GLenum(GL_UNSIGNED_INT),
      // Indices not required because we set GL_ELEMENT_ARRAY_BUFFER
      nil
    )
  }
}

import Clibglfw
import VectorMath

public struct Vertex: Equatable, Hashable {
  public var position: vec3
  public var normal: vec3
  public var textureCoordinate: vec2

  public init(
    position: vec3,
    normal: vec3 = vec3(0, 0, 0),
    textureCoordinate: vec2 = vec2(0, 0)
  ) {
    self.position = position
    self.normal = normal
    self.textureCoordinate = textureCoordinate
  }

  public init(_ x: Float, _ y: Float, _ z: Float) {
    self.init(position: vec3(x, y, z))
  }
}

public func +(lhs: Vertex, rhs: vec3) -> Vertex {
  return Vertex(
    position: lhs.position + rhs,
    normal: lhs.normal,
    textureCoordinate: lhs.textureCoordinate
  )
}

public struct Triangle: Equatable, Hashable {
  public var a: Vertex
  public var b: Vertex
  public var c: Vertex

  public init(_ a: Vertex, _ b: Vertex, _ c: Vertex) {
    self.a = a
    self.b = b
    self.c = c
  }

  public init(_ a: vec3, _ b: vec3, _ c: vec3) {
    self.a = Vertex(position: a)
    self.b = Vertex(position: b)
    self.c = Vertex(position: c)
  }

  mutating func computeNormals() {
    let x = (c.position - a.position).cross(b.position - a.position)
    let normal = x.normalized()
    a.normal = normal
    b.normal = normal
    c.normal = normal
  }
}

import VectorMath

extension Mesh {
  /// Create polygons for a cone with unit height and diameter, centered
  /// at the origin, with separate number of radial subdivisions and
  /// height subdivisions.
  ///
  /// - Parameter radialDivisions: number of subdivisions on the radial base
  /// - Parameter heightDivisions: number of subdivisions along the height
  static func cone(radialDivisions: Int, heightDivisions: Int) -> Mesh {
    let radialDivisions = max(radialDivisions, 3)
    let heightDivisions = max(heightDivisions, 1)

    let m = Mesh()
    let radius: Float = 0.5

    let topCenter = Vertex(position: vec3(0, 0.5, 0))
    let bottomCenter = Vertex(position: vec3(0, -0.5, 0))
    let angleDiff = (2 * .pi) / Float(radialDivisions)
    var angle: Float = 0.0
    while angle < 2 * .pi {
      defer { angle += angleDiff }
      let v1 = alongCircle(-0.5, radius, angle)
      let v2 = alongCircle(-0.5, radius, angle + angleDiff)
      m.addTriangle(Triangle(bottomCenter, v1, v2))

      let diff1 = v1.position - topCenter.position
      let diff2 = v2.position - topCenter.position
      m.addTriangle(Triangle(
        topCenter,
        topCenter + (diff2 / Float(heightDivisions)),
        topCenter + (diff1 / Float(heightDivisions))
      ))

      // Start at 1, making quads as we go down.
      for i in 0..<heightDivisions {
        let h = Float(i)
        let startDiff1 = (diff1 / Float(heightDivisions)) * h
        let startDiff2 = (diff2 / Float(heightDivisions)) * h
        let endDiff1 = (diff1 / Float(heightDivisions)) * (h + 1)
        let endDiff2 = (diff2 / Float(heightDivisions)) * (h + 1)

        m.addQuad(
          topCenter + startDiff2,
          topCenter + endDiff2,
          topCenter + endDiff1,
          topCenter + startDiff1
        )
      }
    }
    return m
  }
}

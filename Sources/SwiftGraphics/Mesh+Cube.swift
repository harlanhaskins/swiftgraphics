import VectorMath

extension Mesh {
  /// Creates a unit cube, centered at the origin, with a given number
  /// of subdivisions in each direction on each face.
  static let cube: Mesh = {
    let m = Mesh()
    let l: Float = 0.5
    // front face
    m.addQuad(
      vec3( l, -l, -l),
      vec3( l,  l, -l),
      vec3(-l,  l, -l),
      vec3(-l, -l, -l),
      inferNormals: true
    )

    // back face
    m.addQuad(
      vec3(-l, -l, l),
      vec3(-l,  l, l),
      vec3( l,  l, l),
      vec3( l, -l, l),
      inferNormals: true
    )

    // left face
    m.addQuad(
      vec3(-l, -l, -l),
      vec3(-l,  l, -l),
      vec3(-l,  l,  l),
      vec3(-l, -l,  l),
      inferNormals: true
    )

    // right face
    m.addQuad(
      vec3(l, -l,  l),
      vec3(l,  l,  l),
      vec3(l,  l, -l),
      vec3(l, -l, -l),
      inferNormals: true
    )

    // top face
    m.addQuad(
      vec3( l, l, -l),
      vec3( l, l,  l),
      vec3(-l, l,  l),
      vec3(-l, l, -l),
      inferNormals: true
    )

    // bottom face
    m.addQuad(
      vec3( l, -l,  l),
      vec3( l, -l, -l),
      vec3(-l, -l, -l),
      vec3(-l, -l,  l),
      inferNormals: true
    )
    return m
  }()
}

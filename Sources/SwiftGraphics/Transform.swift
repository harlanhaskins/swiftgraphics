import VectorMath

public struct Transform {
  public var rotation = Quaternion.identity
  public var scale = vec3(1, 1, 1)
  public var translation = vec3(0, 0, 0)

  mutating func translateBy(_ translation: vec3) {
    self.translation = self.translation + translation
  }

  mutating func rotateBy(_ rotation: vec3) {
    // Compose axis-angle rotations
    let xRot = Quaternion(axisAngle: vec4(1, 0, 0, rotation.x))
    let yRot = Quaternion(axisAngle: vec4(0, 1, 0, rotation.y))
    let zRot = Quaternion(axisAngle: vec4(0, 0, 1, rotation.z))

    self.rotation = yRot * xRot * zRot * self.rotation
  }

  mutating func scaleBy(_ scale: vec3) {
    self.scale = self.scale * scale
  }

  var matrix: mat4 {
    return mat4(translation: translation) *
           mat4(quaternion: rotation) *
           mat4(scale: scale)
  }
}

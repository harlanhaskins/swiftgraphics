import Foundation
import VectorMath

public struct Projection {
  public enum Kind {
    case orthographic
    case frustum
  }
  public var kind: Kind
  public var fieldOfViewY: Float
  public var aspectRatio: Float
  public var near: Float
  public var far: Float

  public var matrix: mat4 {
    switch kind {
    case .frustum:
      return mat4(
        fovy: fieldOfViewY.radians,
        aspect: aspectRatio,
        near: near,
        far: far
      )
    case .orthographic:
      fatalError("unimplemented")
    }
  }
}

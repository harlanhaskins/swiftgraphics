import Clibglfw
import VectorMath

public struct Light {
  public var position: vec3
  public var color: vec3
}

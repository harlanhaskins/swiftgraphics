import Clibglfw
import VectorMath

public class Model: Transformable {
  let mesh: Mesh
  let material: Material
  public var transform = Transform()

  public init(mesh: Mesh, material: Material) {
    self.mesh = mesh
    self.material = material
  }

  func finalize() {
    mesh.finalize()
  }

  func draw(in scene: Scene) {
    let shader = material.shader
    glUseProgram(shader.program)
    let modelTransform = transform.matrix
    let mvpTransform = scene.projection.matrix *
                       scene.camera.viewMatrix *
                       modelTransform
    shader.setUniform(.modelTransform, value: modelTransform)
    shader.setUniform(.mvpTransform, value: mvpTransform)

    let normalTransform = mat3(modelTransform).inverse.transpose
    shader.setUniform(.normalTransform, value: normalTransform)
    shader.setUniform(.cameraPosition, value: scene.camera.transform.translation)
    shader.setUniform(.lightPosition, value: scene.light.position)
    shader.setUniform(.lightColor, value: scene.light.color)

    material.setProperties()

    mesh.draw(in: material.shader)
  }
}

extension Shader.Parameter {
  static var lightPosition: Shader.Parameter<vec3> {
    return .init(rawValue: "lightPosition")
  }
  static var lightColor: Shader.Parameter<vec3> {
    return .init(rawValue: "lightColor")
  }
  static var cameraPosition: Shader.Parameter<vec3> {
    return .init(rawValue: "cameraPosition")
  }
  static var modelTransform: Shader.Parameter<mat4> {
    return .init(rawValue: "modelTransform")
  }
  static var mvpTransform: Shader.Parameter<mat4> {
    return .init(rawValue: "mvpTransform")
  }
  static var normalTransform: Shader.Parameter<mat3> {
    return .init(rawValue: "normalTransform")
  }
}

import VectorMath

public typealias vec2 = Vector2
public typealias vec3 = Vector3
public typealias vec4 = Vector4
public typealias mat3 = Matrix3
public typealias mat4 = Matrix4

extension mat3 {
  init(_ m: mat4) {
    self = .init(
      m.m11, m.m12, m.m13,
      m.m21, m.m22, m.m23,
      m.m31, m.m32, m.m33
    )
  }
}

extension vec3 {
  var radians: vec3 {
    return vec3(x.radians, y.radians, z.radians)
  }
}

extension vec4 {
  var radians: vec4 {
    return vec4(x.radians, y.radians, z.radians, w.radians)
  }
}

extension Float {
  var radians: Float {
    return (self * .pi) / 180
  }
}

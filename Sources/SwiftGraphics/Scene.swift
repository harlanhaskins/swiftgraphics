import Clibglfw
import VectorMath

public class Scene {
  var models = [Model]()
  public var camera: Camera
  public var light: Light
  public var projection: Projection

  public init(camera: Camera, light: Light, projection: Projection) {
    self.camera = camera
    self.light = light
    self.projection = projection
  }

  public func addModel(_ model: Model) {
    model.finalize()
    models.append(model)
  }

  internal func draw() {
    glClear(GLbitfield(GL_COLOR_BUFFER_BIT) | GLbitfield(GL_DEPTH_BUFFER_BIT))
    for model in models {
      model.draw(in: self)
    }
  }
}

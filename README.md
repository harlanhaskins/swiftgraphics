# SwiftGraphics

SwiftGraphcis is a small cross-platform 3D graphics engine written in Swift
using glfw.

## Building

To build, run

```
swift build -Xlinker -L/usr/local/lib
```

to ensure the linker can find glfw.

#version 120

// Phong vertex shader
//
// Contributor:  Harlan Haskins

// Vertex location (in model space)
attribute vec3 vPosition;

// Normal vector at vertex (in model space)
attribute vec3 vNormal;

// Texture coordinate for this vertex
attribute vec2 vTexCoord;

uniform mat4 modelTransform;
uniform mat4 mvpTransform;
uniform mat3 normalTransform;

// add all necessary variables for communicating with
// the fragment shader here

varying vec3 N;
varying vec4 V;

varying vec2 textureCoord;

void main() {

  // Here is where you should all all your code to perform the
  // vertex shader portion of your lighting and shading work
  //
  // remember that transformation of the surface normal should not
  // include translations; see
  // http://www.lighthouse3d.com/tutorials/glsl-tutorial/the-normal-matrix/
  // for a discussion.  normal transformation should be done using the
  // inverse transpose of the upper-left 3x3 submatrix of the modelView
  // matrix.

  vec4 pos = vec4(vPosition, 1);

  N = normalize(normalTransform * vNormal);
  V = modelTransform * pos;

  textureCoord = vTexCoord;

  // Transform the vertex location into clip space
  gl_Position = mvpTransform * pos;
}

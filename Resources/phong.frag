#version 120

varying vec3 N;
varying vec4 V;
varying vec2 textureCoord;

uniform bool useTexture;
uniform sampler2D texture;
uniform vec2 textureScaling;

uniform vec3 cameraPosition;

uniform vec3 lightPosition;
uniform vec3 lightColor;

uniform vec3 materialColor;
uniform float ambientCoeff;
uniform float diffuseCoeff;
uniform float specCoeff;
uniform float specExp;

void main() {
  vec3 vPos = vec3(V / V.w);

  vec3 matColor = materialColor;

  if (useTexture) {
    matColor *= texture2D(texture, textureCoord * textureScaling).xyz;
  }

  vec3 lightDir = normalize(lightPosition - vPos);
  vec3 reflectionDir = reflect(-lightDir, N);
  vec3 viewDir = normalize(cameraPosition - vPos);

  float diffuseMult = max(dot(lightDir, N), 0);
  float specularMult = pow(max(dot(viewDir, reflectionDir), 0), specExp);

  vec3 ambient = lightColor * matColor * ambientCoeff;
  vec3 diffuse = lightColor * matColor * diffuseCoeff * diffuseMult;
  vec3 specular = lightColor * matColor * specCoeff * specularMult;

  gl_FragColor = vec4(ambient + diffuse + specular, 1);
}
